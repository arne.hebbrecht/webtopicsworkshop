# install npm :
# Linux:    
    Ubuntu/Debian:
        bash
        Copy code
        sudo apt update
        sudo apt install nodejs npm
    
    Fedora:
        bash
        Copy code
        sudo dnf install nodejs npm
    
    CentOS/RHEL:
        curl -sL https://rpm.nodesource.com/setup_14.x | sudo bash -
        sudo yum install -y nodejs
        sudo yum install -y npm

# Windows:

    Go to the Node.js website: https://nodejs.org/
    Download the "LTS" (Long-Term Support) version for Windows.
    Run the installer and follow the installation steps.

    or use powershell winget install --id=OpenJS.NodeJS

# macOS:
    Use Homebrew (if you have Homebrew installed):
    bash
    Copy code
    brew install node

    Alternatively, you can download the macOS installer from the official Node.js website and run it


# npm run build
    use this command inside the workshop_realtime_firebase
    the second time you can use this command: npx webpack --mode production
    this is faster you will need to run this command every time you change something 
    on the javascript files 

    this wil only work after you created the firebaseSetup.js file !!!!!!!!!!

# Start the live server on Studio Visual Code and go to public/index.html

# exercise 1 (setup) 
1.	De deelnemer gaat naar deze site : https://firebase.google.com/docs/database hier drukken we op de knop console die rechtsboven te vinden is 
2.	Add project
3.	Kies een naam vb : workshopFirebase-2223
4.	Disable google analytics (delen al genoeg data 😉 )
5.	Create project 
6.	Build  --> Realtime Database
7.	Create database --> location belgium --> next --> start in test mode
8. Build --> authentication --> anoniem
9. user tab --> allow 
10.  `![Alt text](image.png)`
11.	Register app : kies zelf een naam
12.	Kopier de const firebaseConfig deze zal je nodig hebben 

# create the firebaseSetup.js files  inside the ./src/firebaseSetup.js: 
    import { initializeApp } from 'firebase/app';
    import { getAuth } from 'firebase/auth';
    import { getDatabase } from 'firebase/database';

    // Your Firebase project configuration
    const firebaseConfig = {
    apiKey: "",
    authDomain: "",
    databaseURL: "",
    projectId: "",
    storageBucket: "",
    messagingSenderId: "",
    appId: ""
    };

    // Initialize Firebase
    const firebaseApp = initializeApp(firebaseConfig);

    // Use the initialized firebaseApp to get the database and auth objects
    const database = getDatabase(firebaseApp);
    const auth = getAuth(firebaseApp);

    export { database, auth };

# excercise 2 (./user) 

**a_UpdateScoreRealTime.js**
- schrijf hier een onChildchanged die kijkt of de score van de ingelogde user is veranderd 

- tips: gebruik onchildChanged,kijk of de ingelogde user zijn mail in de database staat

- cheatSheet: `![Alt text](image-7.png)`

**b_getUserInputFromRegisterForm.js**
- schrijf hier een listener die luisterd naar de submit van het form op index.html(registrationForm)

- tips : DOMContentLoaded, assync, gebruik encryptpassword.js (hashPassword(password)), stuur door naar addUserToDatabase(email,password)

- cheatSheet: `![Alt text](image-2.png)`


**c_addUser.js**
- hier zullen we een hebruiker proberen aanmaken op de database onder de tabel users 

- tips : https://firebase.google.com/docs/database/web/read-and-write#web-modular-api_7

- cheatSheet: `![Alt text](image-1.png)`
    
**d_ScheckIfUserExists**
- hier zal een check gebeuren die kijkt in de database of het email al bestaat van de gebruiker dit doen we om 
te voorkomen dat er dubbele data in de database terecht kan komen 

- tips : https://firebase.google.com/docs/database/web/read-and-write#web-modular-api_7

- cheatSheet : `![Alt text](image-3.png)`

als alles goed verlopen is zou je nu een gebruiker kunnen aanmaken deze wordt automatisch ingelogd en geregistreerd bij de database alsnog zal het 
password, de username en de score van 0 in de database terechtkomen.

# excercise 3 (./score)

**b_listener.js**
-   zullen opnieuw een listener schrijven de eerste zal luisteren naar de button updateScoreButton en zal de score verhogen met 1 we maken hiervoor gebruik van de functie incrementUserScore().

- de 2de listern zal luisteren naar de deletScoreButton en zal de score van de user terug op 0 zetten we gebruiken hiervoor de fucntie resetDatabasUserScore.

- tips: DOMContentLoaded,document.getElementById,functions

- cheatSheet: `![Alt text](image-4.png)`

**c_incrementUserScore.js**

- hier zie je de fucntie incremenUserScore die enkele dingen uitvoerd :
    1. let email deze zal het email adres opvragen van de user die nu ingelogd is 
    2. let userscore deze zal de huidige score van de user opvragen
    3. hierna wordt de functie updateDataBaseUserScore() opgeropen

- hier mag je terug een conectie maken naar de databas , users en een snapshot creëren hiervan 

- nu moet er code geschreven worden die checked of het email adres bestaat in de database en als dit zo is willen we dat de score geupdate wordt

- tips : RTFM (https://firebase.google.com/docs/database/web/start)

- cheatSheet: `![Alt text](image-5.png)`

**d_resetScore.js**

- als voorgaande code gelukt is zou dit vrij makkelijk moeten zijn aangezien we hier gewoon de code op 0 zetten 
- tips : kijk naar de code van incrementScore.js

- cheatSheet: `![Alt text](image-6.png)`