const path = require('path');
module.exports = {
  entry: {
    register: './src/entryfiles/entryFileRegister.js',
    score: './src/entryfiles/entryFileScore.js',
  },
  output: {
    filename: './js/[name]_bundle.js',
    path: path.resolve(__dirname, 'public'),
  },
}
  