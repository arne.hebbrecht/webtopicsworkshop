function importAll(r) {
    r.keys().forEach(r);
  }
  // Import JavaScript files from the current directory
  importAll(require.context('../', false, /\.js$/));
  // Import JavaScript files from the "user" directory
  importAll(require.context('../score', true, /\.js$/));
  importAll(require.context('../auth', true, /\.js$/));