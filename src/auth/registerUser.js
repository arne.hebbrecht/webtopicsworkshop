import { createUserWithEmailAndPassword } from 'firebase/auth';
import { auth } from '../firebaseSetup';

// Function to register a user
async function registerUser(email, password) {
  try {
    const userCredential = await createUserWithEmailAndPassword(auth, email, password);
    // User has been registered successfully
    const user = userCredential.user;
    console.log('User has been registered:', user);
    // You can access user properties like user.uid, user.email, user.displayName, etc.
    return user;
  } catch (error) {
    // Handle registration error
    const errorCode = error.code;
    const errorMessage = error.message;
    console.error('Error registering user:', errorCode, errorMessage);
    // You can handle the error as needed (e.g., display an error message to the user).
    return null;
  }
}
export { registerUser };
