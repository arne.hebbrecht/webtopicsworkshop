import { signInWithEmailAndPassword } from 'firebase/auth';
import { auth } from '../firebaseSetup';

// Function to sign in a user
async function signInUser(email, password) {
  try {
    // Sign in the user
    await signInWithEmailAndPassword(auth, email, password);
    console.log('User has been signed in.(signInUser.js');

    // You can access user properties if needed
  } catch (error) {
    // Handle sign-in error
    const errorCode = error.code;
    const errorMessage = error.message;
    console.error('Error signing in the user:', errorCode, errorMessage);
    // You can handle the error as needed (e.g., display an error message to the user).
  }
}

export { signInUser };
