import { auth } from '../firebaseSetup';
import { onAuthStateChanged } from 'firebase/auth';


// Define a function to monitor the authentication state
function checkAuthStatus() {
  onAuthStateChanged(auth, (user) => {
    if (user) {
      // User is signed in
      console.log('User is signed in:', user.email);
      // You can access user properties like user.uid, user.email, user.displayName, etc.
    } else {
      // User is signed out
      console.log('User is signed out');
    }
  });
}

function getEmail() {
  onAuthStateChanged(auth, (user) => {
    if (user) {
      // User is signed in
      return user.email;
      // You can access user properties like user.uid, user.email, user.displayName, etc.
    } else {
      // User is signed out
      console.log('User is signed out');
    }
  });
}

function getScore() {
  onAuthStateChanged(auth, (user) => {
    if (user) {
      // User is signed in
      return user.score;
      // You can access user properties like user.uid, user.email, user.displayName, etc.
    } else {
      // User is signed out
      console.log('User is signed out');
    }
  });
}


async function getEmailasc() {
  return new Promise((resolve, reject) => {
    onAuthStateChanged(auth, (user) => {
      if (user) {
        resolve(user.email);
      } else {
        reject('User is signed out');
      }
    });
  });
}

// Call the function to check the authentication status when the page loads
export {checkAuthStatus,getEmail,getScore,getEmailasc};

