import { ref, onChildChanged } from 'firebase/database';
import { database } from '../firebaseSetup'; // Import your Firebase setup
import {getEmail } from '../auth/getUser';

const usernameOrEmail = getEmail();

// Reference to the 'users' node in the database
const usersRef = ref(database, 'users');

// Function to update the HTML content with the user's score
function updateUserScore(score) {
  const userScoreElement = document.getElementById('userScore');
  userScoreElement.textContent = `User Score: ${score}`;
}

// Set up a real-time listener for changes in child nodes
onChildChanged(usersRef, (snapshot) => {
  if (snapshot.exists()) {
    const userData = snapshot.val();

    if (userData.username === usernameOrEmail || userData.email === usernameOrEmail) {
      // User found, retrieve the score
      const userScore = userData.score;
      updateUserScore(userScore); // Update the HTML content
    }
  }
});

