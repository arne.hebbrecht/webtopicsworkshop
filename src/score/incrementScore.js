import { database } from '../firebaseSetup';
import { ref, get, set } from 'firebase/database';
import { getUserScore } from './getScore';
import { getEmailasc } from '../auth/getUser';


async function incrementUserScore( incrementAmount) {
  try {
    let email = await getEmailasc();
    let userScore = await getUserScore(email);
    
    if (userScore !== null) {
      const newScore = userScore + incrementAmount;
      updateDatabaseUserScore(email, newScore);
    } else {
      // Handle the case where the user score is not available
      console.log('User score is not available.');
    }
  } catch (error) {
    console.error('Error incrementing user score:', error);
  }
}

async function updateDatabaseUserScore(email, newScore) {
  try {
    // hier zal een usersRef en snapshot aangemaakt moeten worden 
    //als de snapshot bestaat zullen we door de userdata van de snapshot moeten lopen
    //hier kijken we of het email adres gelijk is aan een email adres van een user in de db
    //hierna zullen we de score van deze user updaten naar de nieuwe score
    //return hierna (functioneert als een exit van de loop)
  } catch (error) {
    console.error('Error updating user score in the database:', error);
  }
}
export { incrementUserScore };
