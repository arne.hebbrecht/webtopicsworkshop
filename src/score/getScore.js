import { ref, get } from 'firebase/database';
import { database } from '../firebaseSetup';
import {getEmail,getScore} from '../auth/getUser';

// Reference to the 'users' node in the database
const usersRef = ref(database, 'users');
const usernameOrEmail = getEmail();

// Function to get the score for a specific user
async function getUserScore(usernameOrEmail) {
  try {
    // Check if the user exists in the database
    const snapshot = await get(usersRef);

    if (snapshot.exists()) {
      let userScore = getScore(); // Initialize userScore variable

      // Loop through the users to find the specific user by username or email
      snapshot.forEach((childSnapshot) => {
        const userData = childSnapshot.val();
        if (userData.username === usernameOrEmail || userData.email === usernameOrEmail) {
          // User found, retrieve the score
          userScore = userData.score;
          
          //load on page
          const userScoreElement = document.getElementById('userScore');
          userScoreElement.textContent = `User Score: ${userScore}`;
        }
      });

      if (userScore !== null) {
        return userScore; // Return the score if user is found
      } else {
        // User not found
        console.log(`User '${usernameOrEmail}' not found.`);
      }
    } else {
      // No users in the database
      console.log('No users in the database.');
    }
  } catch (error) {
    console.error('Error getting user score:', error);
  }
}
getUserScore(usernameOrEmail);
export { getUserScore };
