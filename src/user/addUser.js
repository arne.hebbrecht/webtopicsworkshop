import { push, set, ref } from 'firebase/database';
import { database } from '../firebaseSetup';
import { checkIfUserExists } from './checkIfUserExists';
import { registerUser } from '../auth/registerUser';
import { signInUser } from '../auth/signInUser';

const usersRef = ref(database, 'users');

// addUserToDatabase function
async function addUserToDatabase(email, password) {
  try {

    //dit verwijst naar een stukje dat checkt of de gebruiker al bestaat 
    //dit mag je uncommenten als de if(user) werkt
    // const userExists = await checkIfUserExists(email);
    
    if (!userExists) {
      registerUser(email, password).then(async (user) => {
        if (user) {
          const newUserRef = push(usersRef);
          //shrijf hier de code om een user aan te maken we willen de velden 
          //email 
          //password 
          //score met waarde 0
          signInUser(email,password);

          //redirect to score.html of doe dit handmatig

        } else {
          console.log('Registration failed');
        }
      });
  } 
  else {
      console.log('User already exists.');
    }
  } catch (error) {
    console.error('Error adding new user:', error);
  }
}

export { addUserToDatabase };
export { usersRef };
