async function hashPassword(password) {
    // Generate a random salt for each user (a new salt for each password)
    const salt = crypto.getRandomValues(new Uint8Array(16));
  
    // Use the TextEncoder to encode the password string to bytes
    const encoder = new TextEncoder();
    const passwordData = encoder.encode(password);
  
    // Combine the password data and the salt
    const combinedData = new Uint8Array(passwordData.length + salt.length);
    combinedData.set(passwordData, 0);
    combinedData.set(salt, passwordData.length);
  
    // Hash the combined data using a one-way hashing algorithm (e.g., SHA-256)
    const hashBuffer = await crypto.subtle.digest('SHA-256', combinedData);
  
    // Convert the hash to a hexadecimal string
    const hashArray = Array.from(new Uint8Array(hashBuffer));
    const passwordHash = hashArray.map(byte => byte.toString(16).padStart(2, '0')).join('');
  
    return passwordHash;
}
export{hashPassword};